package main

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/labstack/echo"
	"github.com/mailru/easyjson"
	"github.com/stretchr/testify/assert"
	"github.com/tidwall/gjson"
)

type mockDB struct {
	interval time.Duration
}

func (mdb mockDB) saveBatch() {
	return
}

func (mdb mockDB) startQueueConsumer() chan Msg {
	return make(chan Msg)
}

func (mdb mockDB) getIntervalSample(string, int64, int64) (sample, error) {
	return sample{}, nil
}

func (mdb mockDB) query(string) error {
	return nil
}

func (mdb mockDB) getSeries(tag string, start int64, end int64) (APIResponse, error) {
	mockSample := sample{1000, []float64{1, 2, 3}}

	return APIResponse{tag, start, end, []sample{mockSample}}, nil
}

func TestGetSeries(t *testing.T) {
	t.Parallel()

	db := mockDB{}

	router := gin.New()
	router.GET("/api", apiHandler(db))

	ts := httptest.NewServer(router)
	defer ts.Close()

	resp, err := http.Get(ts.URL + "/api?start=999&end=1001&tag=test_tag2")

	assert.NoError(t, err)

	var data APIResponse

	if e := json.NewDecoder(resp.Body).Decode(&data); e != nil {
		t.Fatal("error decoding API response", e, resp.Body)
	}

	mockSample := sample{1000, []float64{1, 2, 3}}

	expected := APIResponse{"test_tag2", 999, 1001, []sample{mockSample}}

	assert.Equal(t, expected, data, "resp body should match")
	assert.Equal(t, "200 OK", resp.Status, "should get a 200")
}

func TestSave(t *testing.T) {
	t.Parallel()

	msgChan := make(chan Msg, 200000)

	router := gin.New()
	router.POST("/save", saveHandler(msgChan))

	ts := httptest.NewServer(router)
	defer ts.Close()

	testMessage := Msg{1000, "test_tag", []float64{1.1}}

	jsonMsg, _ := json.Marshal(testMessage)

	resp, err := http.Post(ts.URL+"/save", "application/json", bytes.NewReader(jsonMsg))

	defer resp.Body.Close()
	assert.NoError(t, err)

	_, ioerr := ioutil.ReadAll(resp.Body)
	assert.NoError(t, ioerr)
	// assert.Equal(t, "test_tag", string(body), "resp body should return tag")
	assert.Equal(t, "200 OK", resp.Status, "should get a 200")
	assert.Equal(t, testMessage, <-msgChan, "should appear in chan")
}

func TestHealth(t *testing.T) {
	t.Parallel()

	router := gin.New()
	router.GET("/health", healthCheck)

	ts := httptest.NewServer(router)
	defer ts.Close()

	resp, err := http.Get(ts.URL + "/health")
	defer resp.Body.Close()

	assert.NoError(t, err)

	body, ioerr := ioutil.ReadAll(resp.Body)
	assert.NoError(t, ioerr)
	assert.Equal(t, "OK", string(body), "should return 'OK'")
}

func BenchmarkEasyJSON(b *testing.B) {
	gin.SetMode(gin.ReleaseMode)

	router := gin.New()
	router.POST("/bench", func(c *gin.Context) {
		var m Msg

		body, ioerr := ioutil.ReadAll(c.Request.Body)

		if ioerr != nil {
			log.Panic("> error reading body", ioerr)
		}

		if err := easyjson.Unmarshal(body, &m); err != nil {
			log.Panic("> error decoding json", err)
		}

		c.String(200, m.Tag)
	})

	jsonMsg, _ := json.Marshal(Msg{time.Now().UnixNano(), "tag", []float64{1.2, 2.3, 3.4, 4.5, 5.1, 6.89, 7.0, 8.12, 9.99, 10.10}})

	w := httptest.NewRecorder()

	for i := 0; i < b.N; i++ {
		req, _ := http.NewRequest("POST", "/bench", bytes.NewReader(jsonMsg))

		router.ServeHTTP(w, req)
	}
}

func BenchmarkGinJSON(b *testing.B) {
	gin.SetMode(gin.ReleaseMode)

	router := gin.New()
	router.POST("/bench", func(c *gin.Context) {
		var m Msg

		if err := c.BindJSON(&m); err != nil {
			log.Print(">>> can't parse incoming message: ", err, c.Request, c.Request.Body)
		}

		c.String(200, m.Tag)
	})

	jsonMsg, _ := json.Marshal(Msg{time.Now().UnixNano(), "tag", []float64{1.2, 2.3, 3.4, 4.5, 5.1, 6.89, 7.0, 8.12, 9.99, 10.10}})
	// fmt.Printf("> %+q\n", jsonMsg)

	w := httptest.NewRecorder()

	for i := 0; i < b.N; i++ {
		req, _ := http.NewRequest("POST", "/bench", bytes.NewReader(jsonMsg))

		router.ServeHTTP(w, req)
	}
}

func BenchmarkGJSON(b *testing.B) {
	gin.SetMode(gin.ReleaseMode)
	gjson.UnmarshalValidationEnabled(false)

	router := gin.New()
	router.POST("/bench", func(c *gin.Context) {
		var m Msg

		body, ioerr := ioutil.ReadAll(c.Request.Body)

		if ioerr != nil {
			log.Panic("> error reading body", ioerr)
		}

		if err := gjson.Unmarshal(body, &m); err != nil {
			log.Panic("> error decoding json", err)
		}

		c.String(200, m.Tag)
	})

	jsonMsg, _ := json.Marshal(Msg{time.Now().UnixNano(), "tag", []float64{1.2, 2.3, 3.4, 4.5, 5.1, 6.89, 7.0, 8.12, 9.99, 10.10}})
	// fmt.Printf("> %+q\n", jsonMsg)

	w := httptest.NewRecorder()

	for i := 0; i < b.N; i++ {
		req, _ := http.NewRequest("POST", "/bench", bytes.NewReader(jsonMsg))

		router.ServeHTTP(w, req)
	}
}

func BenchmarkEchoJSON(b *testing.B) {
	jsonMsg, _ := json.Marshal(Msg{time.Now().UnixNano(), "tag", []float64{1.2, 2.3, 3.4, 4.5, 5.1, 6.89, 7.0, 8.12, 9.99, 10.10}})

	e := echo.New()

	e.POST("/bench", func(c echo.Context) (err error) {
		var m Msg

		if err = c.Bind(m); err != nil {
			return
		}

		return c.JSON(http.StatusOK, m.Tag)
	})

	w := httptest.NewRecorder()

	for i := 0; i < b.N; i++ {
		req, _ := http.NewRequest("POST", "/bench", bytes.NewReader(jsonMsg))

		e.ServeHTTP(w, req)
	}
}

func BenchmarkJSON(b *testing.B) {
	var m Msg
	jsonMsg, _ := json.Marshal(Msg{time.Now().UnixNano(), "tag", []float64{1.2, 2.3, 3.4, 4.5, 5.1, 6.89, 7.0, 8.12, 9.99, 10.10}})

	for i := 0; i < b.N; i++ {

		if err := json.Unmarshal([]byte(jsonMsg), &m); err != nil {
			log.Panic("> error decoding json", err)
		}
	}
}

func BenchmarkGJSON_(b *testing.B) {
	var m Msg
	jsonMsg, _ := json.Marshal(Msg{time.Now().UnixNano(), "tag", []float64{1.2, 2.3, 3.4, 4.5, 5.1, 6.89, 7.0, 8.12, 9.99, 10.10}})

	for i := 0; i < b.N; i++ {

		if err := gjson.Unmarshal([]byte(jsonMsg), &m); err != nil {
			log.Panic("> error decoding json", err)
		}
	}
}

func BenchmarkEJSON_(b *testing.B) {
	var m Msg
	jsonMsg, _ := json.Marshal(Msg{time.Now().UnixNano(), "tag", []float64{1.2, 2.3, 3.4, 4.5, 5.1, 6.89, 7.0, 8.12, 9.99, 10.10}})

	for i := 0; i < b.N; i++ {

		if err := easyjson.Unmarshal([]byte(jsonMsg), &m); err != nil {
			log.Panic("> error decoding json", err)
		}
	}
}
