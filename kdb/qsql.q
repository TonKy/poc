/ use namespace .P for all defined functions

/ generate 'amt' symbols for tags distribution
/ .P.gen_syms:{[amt] (`$"t" ,/: string til amt)}

.P.tags: (`$"t" ,/: string til 10000)

/ empty table definition
.P.gen_t:{([] tag:`symbol$(); ts:`s#`timestamp$(); val:())}
.P.gen_tl: {([] tag:`symbol$(); ts:`s#`long$(); val:())}

/ generate enumerated tags
/ .P.gen_tags:{[amt;tags] `sym$amt?tags}

/ create 'amt' records for a tag, distributed randomly for 24h, starting with 'end'-24h, usually a .z.P - local timestamp
.P.gen_ts_24h:{[amt;end] asc (end-24:00:00) + amt?`timespan$24:00:00}

/ create 'amt' floating points
.P.gen_vals:{x?10.0}

/ generate 'historical'(24h worth) of amt records till 'end' - 24h
/ .P.gen_recs_24h: {[amt;end;tags] ([] tag:amt?.P.tags; ts:.P.gen_ts_24h[amt;end]; val:.P.gen_vals[amt])}

/ generate amt records starting _now_
.P.gen_row:{[amt;tags] ([] tag:amt?tags; ts:`long$(.z.p - `long$1970.01.01D00:00:00.000000) + til amt; val:{10?1.0} each til amt)}

.P.gen_recs: {[amt;tags] batch_size:amt&1000; .tmp.gen: .P.gen_tl[]; do[amt div batch_size; `.tmp.gen upsert .P.gen_row[batch_size;tags]]; .tmp.gen}

/ .P.interval:{`long$((`timestamp$y) - (`timestamp$x)) % 100}
.P.interval:{`long$(y - x) % 100}

/ find last value in 100 equal time buckets using xbar, too slow atm
.P.downsample:{[tbl;s;e] 1_ select by .P.interval[s;e] xbar ts from tbl where ts>s,ts<=e}

/ downsample with implicit reload
.P.downsample_tag:{[tag;s;e] system"l ", "/tmp/db/"; .P.downsample_aj[select from t where int=`int$`sym?tag; tag; s; e]}

/ find last value for a tag in a table, split in 100 buckets by time. table should use `g on tag column!
.P.gen_ts_int:{[s;e] i:.P.interval[s;e]; s + i* (1 + til 100)}
.P.join_on:{[tag;s;e] ([] tag:`sym$tag; ts:.P.gen_ts_int[s;e])}

.P.downsample_aj:{[tbl;tag;s;e] aj[`tag`ts;.P.join_on[tag;s;e];tbl]}

/ ignore tag
.P.join_on_notag:{[s;e] ([] ts:.P.gen_ts_int[s;e])}
.P.downsample_aj_notag:{[tbl;s;e] aj[`ts;.P.join_on_notag[s;e];tbl]}

/ bulk append records to a table

/ save single splayed table to disk

/ save partitioned tag to a separate db
.P.extr:{[tbl;tg] select from tbl where tag=`sym$tg}
.P.path:{`$raze ":/tmp/db/", string(`int$`sym$x), "/t/"}
.P.fpath:{"/tmp/db/", string(`int$`sym$x)}
.P.save_tag:{[tbl;tg] .P.path[tg] upsert .P.extr[tbl;tg]}

/ save all tags to respective dbs
.P.upsert_all:{tenum: .Q.en[`:/tmp/db/] x; .P.save_tag[tenum] peach distinct tenum[`tag]}

/ downsample last 24h
.P.ds24:{[tbl;tg] .P.downsample_aj_notag[select from tbl where int=`int$`sym$tg; .z.P-24:00:00; .z.P]}

.P.write_batch_fpath:{.P.upsert_all gen_tl[] upsert value "c"$read1`$":", x}

.P.recent_recs: {select tag, ts, val from t where int=`int$`sym$x, ts>(`long$.z.p - `long$1970.01.02D00:00:00.000000)}
.P.prune_tag: {rr: .P.recent_recs[x]; system"rm -rf ", .P.fpath[x]; .P.upsert_all rr}
.P.prune_tags: {.P.prune_tag each x}

/ tickerplant persist to db function
.P.tp_upsert: {.tmp.upd: .tmp.t; .tmp.t: .P.gen_tl[]; .P.upsert_all .tmp.upd; delete upd from `.tmp}
.P.tp_add:{show count x; `.tmp.t upsert x}

/ hdb reload db and update syms for client queries
.P.reload_hdb: {system"l ", "/tmp/db/"}

.tmp.t: .P.gen_tl[]

/ =========================================================

/ \t 250


/ \d .P

/ \ts show t: gen_recs_24h[1*1000;.z.P;.P.tags]
/ \ts upsert_all[t]
/ \l /tmp/db / loads 't' into memory
/ \ts .P.downsample_aj_notag[select from t where int=`int$`sym$`t1234; .z.P-24:00:00; .z.P]

/ \ts .P.downsample[t; `sym$`tag9; .z.P-24:00:00; .z.P]
/ \ts .P.downsample_aj[t; `.P.sym$`tag5; .z.P-24:00:00; .z.P]

/ b1: 

/
t10m: gen_recs[tags;2000.01.01D00:00:00.00000;2000.01.02D00:00:00.00000;10000000]
t10m: gen_recs[tags;2000.01.01D00:00:00.00000;2000.01.02D00:00:00.00000;10000000]
`:/db/t10m/ set .Q.en[`:/db;] t10m
`:/db/1/t/ set .Q.en[`:/db;] extr[tt; `tag1]
`:/db/1/t/ upsert (`sym$`tag1; `timestamp$2000; 2.1)

/ sym: `tag0`tag1`tag2`tag3`tag4`tag5`tag6`tag7`tag8`tag9
sym:{(`$"tag" ,/: string til x)}[10000]
start: `timestamp$0
end: `timestamp$`date$1

.t.interval:{`long$((`timestamp$y) - (`timestamp$x)) % 100}
.t.amt: 86400 * 10 * count sym  / 8.64M per tag

.t.res:{[tbl;tag;s;e] select by .t.interval[s;e] xbar ts from tbl where tag=`sym$tag,ts>s,ts<=e}

.t.fnd:{[s;e] i:.t.interval[s;e]; ts:s + i* til 100}

.t.join_on:{[tag;s;e] ([] tag:`sym$tag; ts:.t.fnd[s;e])}

.t.res2:{[tbl;tag;s;e] aj[`tag`ts;.t.join_on[tag;s;e];tbl]}

/ \ts t:([] sym:`g#`sym$.t.amt?sym; ts:asc .t.amt?end; .t.amt?10.0)
/ \ts t:([] sym:`g#$.t.amt?sym; ts:asc 2001.01.01D13:00 + .t.amt?end; .t.amt?10.0)
gen_recs:{[tags;start;ts;amt] ([] sym:`sym$amt?tags; ts:asc start + amt?ts; amt?10.0)}

/ \ts t1:gen_recs[sym;2000.01.01D00:00:00.00000;00:10:00.00000;600*10000]
/ \ts t2:gen_recs[sym;2000.01.01D00:10:00.00000;D00:20:00.00000;600*10000]

/ \ts .t.res[`tag2;start;end]
/ \ts .t.res2[t1`tag3;start;end]
/ gen_cols: {[amt] ([] ts: asc amt?`timestamp$2000.01.02; tag0:amt?1.0; tag1:amt?1.0; tag2:amt?1.0; tag3:amt?1.0; tag4:amt?1.0)
/ .z.pp:{show x[0]; value x[0]; "HTTP/1.1 200 OK\nContent-Length: 0\nConnection: close\n\n"}
