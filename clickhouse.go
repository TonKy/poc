package main

import (
	"fmt"
	"log"
	"sort"
	"time"

	clickhouse "github.com/roistat/go-clickhouse"
)

// ClickhouseDB let's try it!
type ClickhouseDB struct {
	conn   *clickhouse.Conn
	dbName string
	out    chan []clickhouse.Row
}

func (db ClickhouseDB) saveBatch() {
	for rows := range db.out {
		s := time.Now()

		query, err := clickhouse.BuildMultiInsert("points", clickhouse.Columns{"date", "time", "tag", "values"}, rows)

		if err != nil {
			log.Fatal("!> error creating insert query: ", err)
		}

		if err := query.Exec(db.conn); err != nil {
			log.Fatal("!> error committing to db: ", err)
		}

		log.Printf("> %v sent %d rows", time.Now().Sub(s), len(rows))
	}
}

func (db ClickhouseDB) startQueueConsumer() chan Msg {
	msgChan := make(chan Msg, 100000)
	var rows []clickhouse.Row

	// parallel inserts from `db.out`
	// go db.saveBatch()
	go db.saveBatch()

	// timer := time.Tick(500 * time.Millisecond)
	timer := time.Tick(time.Second)

	go func() {
		for {
			select {
			case <-timer:
				if len(rows) == 0 {
					fmt.Printf(".")
					continue
				}

				db.out <- rows

				log.Printf("> row batch added with tick: %d -> %d, msg chan now: %d\n", len(rows), len(db.out), len(msgChan))

				rows = []clickhouse.Row{}
			case m := <-msgChan:
				s := time.Now()
				today := fmt.Sprintf("%d-%02d-%02d", s.Year(), s.Month(), s.Day())

				rows = append(rows, clickhouse.Row{today, m.Time, m.Tag, m.Values})
			}
		}
	}()

	return msgChan
}

func (db ClickhouseDB) getIntervalSample(tag string, start int64, end int64) (sample, error) {
	var t int64
	var values []float64

	q := fmt.Sprintf("SELECT time, values FROM points where tag='%s' and time >= %d and time <= %d limit 1", tag, start, end)
	query := clickhouse.NewQuery(q)

	fmt.Println("> asking for interval: ", q)

	iter := query.Iter(db.conn)

	if !iter.Scan(&t, &values) {
		return sample{}, iter.Error()
	}

	fmt.Printf("> parsed: %v, %+v\n", t, values)

	return sample{t, values}, nil
}

func (db ClickhouseDB) getSeries(tag string, start int64, end int64) (APIResponse, error) {
	s := time.Now()

	var samples Samples
	var pointTime int64
	var pointValues []float64

	q := fmt.Sprintf("SELECT intDiv(time - start - 1, ((%d AS end) - (%d AS start)) / (100 AS num_chunks) AS step) * step + start + 1 AS interval, argMax(values, time) FROM points WHERE tag='%s' and time BETWEEN start AND end GROUP BY interval ORDER BY interval", end, start, tag)

	query := clickhouse.NewQuery(q)

	iter := query.Iter(db.conn)

	for iter.Scan(&pointTime, &pointValues) {
		samples = append(samples, sample{pointTime, pointValues})
	}

	if iter.Error() != nil {
		fmt.Println("> error getting interval", iter.Error())
	}

	sort.Sort(samples)

	if time.Now().Sub(s) > time.Millisecond*90 {
		fmt.Printf("!> %v \t query took over 90ms: %s, %v, %v\n\n%s\n\n", time.Now().Sub(s), tag, start, end, q)
	}

	return APIResponse{tag, start, end, samples}, nil
}

func getClickhouseDB(dbName string) Database {
	var schema = `create table if not exists points (date Date, time UInt64, tag String, values Array(Float64)) engine = MergeTree(date, time, (tag, time), 4096);`

	conn := clickhouse.NewConn("localhost:8123", clickhouse.NewHttpTransport())

	query := clickhouse.NewQuery(schema)

	if err := query.Exec(conn); err != nil {
		log.Fatal("!> can't execute table query: ", err)
	}

	return ClickhouseDB{conn, dbName, make(chan []clickhouse.Row, 5)}
}

func (db ClickhouseDB) query(q string) error {
	return nil
}
