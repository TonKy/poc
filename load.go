package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"math/rand"
	"net/http"
	"os"
	"runtime"
	"strconv"
	"sync"
	"time"

	"github.com/mailru/easyjson"
)

// HTTPLoader send load over http
type HTTPLoader struct {
	url      string
	workers  *sync.WaitGroup
	messages chan Msg
	counter  int
	numTags  int
	interval time.Duration
}

func postWorker(messages <-chan Msg, wg *sync.WaitGroup, url string) {
	defer wg.Done()

	for msg := range messages {
		sendPostMessage(url, msg)
	}
}

func newHTTPLoader(url string, numTags int) HTTPLoader {
	if url == "" {
		log.Fatal("! empty url for HTTPLoader")
	}

	postMessages := make(chan Msg, 200000)

	numPostWorkers := runtime.NumCPU() * 2

	var wg sync.WaitGroup

	for i := 0; i < numPostWorkers; i++ {
		wg.Add(1)
		go postWorker(postMessages, &wg, url)
	}

	fmt.Printf("> %v, \t added %d 'msgQueue -> POST' workers \n", time.Now(), numPostWorkers)

	return HTTPLoader{url, &wg, postMessages, 0, numTags, time.Hour * 24}
}

func (l *HTTPLoader) add(amt int) {
	start := time.Now()

	for i := 0; i < amt; i++ {
		l.messages <- randMsg(time.Now().UnixNano(), fmt.Sprintf("t%d", i%l.numTags))
	}

	l.counter += amt

	fmt.Printf("> %v, \t %v \t HTTPLoader.add()ed %d messages, queue %d\n", time.Now(), time.Now().Sub(start), amt, len(l.messages))
}

func (l HTTPLoader) stop() {
	close(l.messages)

	log.Printf("> %v \t stopping loader, closed messages chan\n", time.Now())

	l.workers.Wait()

	log.Printf("> %v \t all workers done\n", time.Now())
}

func sendPostMessage(url string, msg Msg) {
	// fmt.Println("> sending message to url: ", url, msg)
	messageJSON, _ := easyjson.Marshal(msg)

	resp, err := http.Post(url, "application/json; charset=utf-8", bytes.NewBuffer(messageJSON))

	if err != nil {
		fmt.Println(err)
	}

	// log.Printf("> post worker sent %v to %v", msg, url)

	io.Copy(ioutil.Discard, resp.Body)
	resp.Body.Close()
}

func envParams(rateEnv string, durationEnv string, urlEnv string) (int, time.Duration, string) {
	rate := 10000
	duration := time.Second * 10
	url := os.Getenv(urlEnv)

	if r, err := strconv.Atoi(os.Getenv(rateEnv)); err == nil && r > 0 {
		rate = r
	}

	if d, err := strconv.Atoi(os.Getenv(durationEnv)); err == nil && d > 0 {
		duration = time.Second * time.Duration(d)
	}

	return rate, duration, url
}

func apiResponseTimes(url string, start int64, end int64, numTags int, stats map[string]int) {
	calls := 10

	responseTimes := make(chan time.Duration, calls)

	var wg sync.WaitGroup

	for i := 0; i < calls; i++ {
		wg.Add(1)

		go func(callStart time.Time) {
			defer wg.Done()

			tag := fmt.Sprintf("t%d", rand.Intn(numTags))

			_, err := getData(url+"/api", start, end, tag)

			if err != nil {
				fmt.Println("!> expected API response for: ", url+"/api", tag, start, end)
				return
			}

			// fmt.Println("> api response: ", tag, start, end, resp)

			callEnd := time.Now().Sub(callStart)

			responseTimes <- callEnd
		}(time.Now())

		time.Sleep(50 * time.Millisecond)
	}

	wg.Wait()

	close(responseTimes)

	for rt := range responseTimes {
		// log.Printf("> rt: %v", rt)

		if rt > 99*time.Millisecond {
			stats["100+"]++
		}

		if 50*time.Millisecond <= rt && rt <= 99*time.Millisecond {
			stats["99"]++
		}

		if 10*time.Millisecond <= rt && rt < 50*time.Millisecond {
			stats["50"]++
		}

		if rt < 10*time.Millisecond {
			stats["10"]++
		}
	}
}

func getData(url string, start int64, end int64, tag string) (APIResponse, error) {
	var data APIResponse

	fullURL := fmt.Sprintf("%s?start=%d&end=%d&tag=%s", url, start, end, tag)

	r, err := http.Get(fullURL)

	if err != nil {
		log.Fatalf("!> error getting API response from %v, %v, %v, %v ", fullURL, start, end, tag)
		return data, err
	}

	body, err := ioutil.ReadAll(r.Body)
	r.Body.Close()

	if err != nil {
		log.Fatal("> error reading body from API response")
		return data, err
	}

	// log.Printf(">>> api response body: %q\n", body)

	e := json.Unmarshal(body, &data)

	if e != nil {
		log.Println("> error unmarshalling API response body to JSON", body)
		return data, err
	}

	return data, nil
}

func randMsg(t int64, tag string) Msg {
	var values []float64

	for i := float64(0); i < 10; i++ {
		values = append(values, float64(time.Now().UnixNano())/10e12)
	}

	return Msg{t, tag, values}
}
