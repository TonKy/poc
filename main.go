package main

import (
	"fmt"
	"log"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/mailru/easyjson"
)

// Msg provides a struct to be used within the app
// incoming messages are parsed into it and converted
// to DB specific formats
type Msg struct {
	Time   int64     `json:"time"`
	Tag    string    `json:"tag"`
	Values []float64 `json:"values"`
}

func main() {
	db := getDB("jet")

	msgChan := db.startQueueConsumer()

	defer close(msgChan)

	r := gin.New()

	r.POST("/save", saveHandler(msgChan))
	r.GET("/api", apiHandler(db))
	r.GET("/health", healthCheck)

	// ginpprof.Wrap(r)

	log.Println("Starting Gin server on 0.0.0.0:8080")

	r.Run(":8080") // listen and serve on 0.0.0.0:8080
}

func healthCheck(c *gin.Context) {
	c.String(200, "OK")
}

func apiHandler(db Database) gin.HandlerFunc {
	// serialize access to client queries, preventing
	// concurrent reads. current implementation reloads
	// kdb+ table on each client query, and they don't
	// seem to play well on concurrent reloads
	ready := make(chan bool, 1)
	ready <- true

	return func(c *gin.Context) {
		<-ready
		defer func() {
			ready <- true
		}()

		startStr := c.Query("start")
		endStr := c.Query("end")
		tag := c.Query("tag")

		start, err := strconv.ParseInt(startStr, 10, 64)

		if err != nil {
			c.JSON(400, err)
			return
		}

		end, err := strconv.ParseInt(endStr, 10, 64)

		if err != nil {
			c.JSON(400, err)
			return
		}

		res, err := db.getSeries(tag, start, end)

		if err == nil {
			c.JSON(200, res)
		} else {
			fmt.Printf("!> apiHandler db.getSeries failed for ?tag=%v&start=%s&end=%s\n", tag, startStr, endStr)
			c.JSON(401, err)
		}

	}
}

// parse incoming json messages and put them on `msgChan` for further
// processing to DB specific structures and batching
func saveHandler(msgChan chan Msg) gin.HandlerFunc {
	return func(c *gin.Context) {
		var m Msg

		if err := easyjson.UnmarshalFromReader(c.Request.Body, &m); err != nil {
			log.Panic("> error decoding json", err)
		}

		msgChan <- m

		c.String(200, "OK")
	}
}
