#!/bin/bash
# ab -k -c 20 -n 1000000 -p save_message.json -T application/json 'http://localhost:8080/save'
# RATE=20000 DURATION=10 URL=http://localhost:8080/save go test -run TestExternal
# influxd restore -database jet -datadir /mnt/ramdisk/influxdb/data -metadir /mnt/ramdisk/influxdb/meta ~/go/src/poc/86m_100tags_backup
# time curl -G 'http://localhost:8086/query?db=jet' --data-urlencode 'q=SELECT last("values") FROM "instruments" where "tagName" = $tagName and time > now() - 24h group by time(864s)' --data-urlencode 'params={"tagName":"tag1"}' > /dev/null
# create keyspace jet WITH REPLICATION = { 'class' : 'SimpleStrategy', 'replication_factor' : 1 };
# create table points (ts bigint, tag text, values set<double>, primary key (tag, ts)) with clustering order by (ts desc) and compaction = { 'class' : 'SizeTieredCompactionStrategy', 'min_threshold': '2', 'max_threshold': '40' };
# time curl -G 'http://localhost:8086/query?db=t' --data-urlencode 'q=SELECT last("value") FROM "point" where "tagName" = $tagName and time > $start and time < $end group by time(864s)' --data-urlencode 'params={"tagName":"tag8","start":"2016-01-01","end":"2016-01-03"}'
# wrk -d 5s -s post.lua http://172.31.31.155:8080/save
# rlwrap -r ~/q/l32/q kdb/tp.q -s 16 -p 6012
# rlwrap -r ~/q/l32/q kdb/hdb.q -s 4 -p 6013

mkdir ~/.ssh

export GOPATH="/home/ubuntu/go"
export PATH="$PATH:$GOPATH/bin"

mkdir -p $GOPATH/src/poc
mkdir -p $GOPATH/bin

# sudo sysctl net.ipv4.tcp_tw_recycle=1
# sudo sysctl net.ipv4.tcp_tw_reuse=1
echo "net.ipv4.tcp_tw_recycle = 1" | sudo tee -a /etc/sysctl.conf
echo "net.ipv4.tcp_tw_reuse = 1" | sudo tee -a /etc/sysctl.conf
sudo sysctl --system

sudo dpkg --add-architecture i386
sudo apt-get update -y

sudo apt-get install -y linux-image-extra-$(uname -r) linux-image-extra-virtual
sudo apt-get install -y apt-transport-https ca-certificates curl software-properties-common

sudo add-apt-repository ppa:longsleep/golang-backports -y

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository -y "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"

curl -sL https://repos.influxdata.com/influxdb.key | sudo apt-key add
sudo add-apt-repository -y "deb https://repos.influxdata.com/ubuntu $(lsb_release -cs) stable"

sudo curl -L "https://github.com/docker/compose/releases/download/1.13.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose

curl https://www.apache.org/dist/cassandra/KEYS | sudo apt-key add -
sudo add-apt-repository "deb http://www.apache.org/dist/cassandra/debian 39x main"

echo "deb http://repo.yandex.ru/clickhouse/xenial stable main" | sudo tee -a /etc/apt/sources.list.d/clickhouse.list
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv E0C56BD4    # optional

sudo apt-get update -y
sudo apt-get install -y docker-ce golang-go git htop apache2-utils influxdb openssh-client fio \
cassandra clickhouse-client clickhouse-server-common wrk libc6:i386 libstdc++6:i386 rlwrap atop

# sudo docker run --name influx -p 8086:8086 -d influxdb
# sudo systemctl start influxdb
sudo gpasswd -a ${USER} docker

go get -u github.com/golang/dep/...

ssh-keygen -f ~/.ssh/id_rsa -N ""
cat ~/.ssh/id_rsa.pub

echo "GET TO THA CHOPPAH! https://bitbucket.org/TonKy/poc/admin/access-keys/"
read

git clone --depth 1 git@bitbucket.org:TonKy/poc.git $GOPATH/src/poc

echo "export GOPATH=$GOPATH" >> ~/.bashrc
echo "export PATH=$PATH:$GOPATH/bin" >> ~/.bashrc
echo "cd $GOPATH/src/poc" >> ~/.bashrc
echo "alias q='QHOME=~/q rlwrap -r ~/q/l32/q'" >> ~/.bashrc

# source ~/.bashrc

cd $GOPATH/src/poc
dep ensure
